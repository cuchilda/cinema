# Frontend
FROM node:10.15 AS npm-builder
WORKDIR /app
COPY ./frontend ./
RUN npm install
EXPOSE 3000
CMD [ "npm", "start" ]

# Backend
FROM python:3.8 as active-server
ENV PYTHONUNBUFFERED=1
WORKDIR /cinema
COPY ./backend-django/cinema/requirements.txt /cinema
RUN pip install -r requirements.txt
COPY backend-django/cinema/ /cinema

