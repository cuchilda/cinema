from rest_framework import serializers
from billboard.models import CinematographicGenre, Film


class CinematographicGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = CinematographicGenre
        fields = ('name', )


class FilmSerializer(serializers.ModelSerializer):
    cinematographic_genre = CinematographicGenreSerializer(many=True)

    class Meta:
        model = Film
        fields = '__all__'


class PremiereListSerializer(serializers.Serializer):
    weekly_films = FilmSerializer(many=True)
    monthly_films = FilmSerializer(many=True)
