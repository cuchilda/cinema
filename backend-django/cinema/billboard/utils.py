from datetime import date, datetime, timedelta
import calendar


def get_monday_sunday_week_today(date_value):
    """
    Return monday date and sunday date given a date:
    :param date_value: Date
    """
    weekday = date_value.weekday()
    monday = date_value - timedelta(weekday)
    sunday = date_value + timedelta((6-weekday))

    return monday, sunday


def get_first_last_day_actual_month(date_value):
    """
    Return first and last day dates of the month given a date.
    :param date_value: Date
    """
    _, number_days_month = calendar.monthrange(date_value.year, date_value.month)
    first_day = datetime(date_value.year, date_value.month, 1)
    last_day = datetime(date_value.year, date_value.month, number_days_month)

    return first_day, last_day
