from django.contrib import admin
from billboard.models import CinematographicGenre, Film


@admin.register(CinematographicGenre)
class CinematographicGenreAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name', )
    list_filter = ('name', )


@admin.register(Film)
class FilmAdmin(admin.ModelAdmin):
    list_display = ('title', 'director', 'active')
    search_fields = ('title', 'active')
    list_filter = ('title', 'cinematographic_genre', 'director', 'active')
