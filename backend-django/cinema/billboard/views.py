from datetime import date
from rest_framework.decorators import api_view
from rest_framework.response import Response

from billboard.models import CinematographicGenre, Film
from billboard.serializers import FilmSerializer, PremiereListSerializer
from billboard.utils import get_first_last_day_actual_month, get_monday_sunday_week_today

@api_view(['GET'])
def film_list(request):
    """
    List all films.
    """
    if request.method == 'GET':
        snippets = Film.objects.all()
        serializer = FilmSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def week_month_premieres(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        today = date.today()
        monday, sunday = get_monday_sunday_week_today(today)

        first_day, last_day = get_first_last_day_actual_month(today)

        weekly_films = Film.objects.filter(
            release_date__gte=monday, release_date__lte=sunday).all()
        monthly_films = Film.objects.filter(
            release_date__gte=first_day, release_date__lte=last_day).all()
        serializer = PremiereListSerializer(
            {
                'weekly_films': weekly_films,
                'monthly_films': monthly_films,
             }
        )
        return Response(serializer.data)
