from django.urls import path

from billboard import views


urlpatterns = [
    path('film-list', views.film_list, name='film-list'),
    path('week-month-premieres', views.week_month_premieres, name='week-month-premieres'),
]
