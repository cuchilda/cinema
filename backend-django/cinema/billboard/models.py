from django.db import models
from cast.models import Person


class CinematographicGenre(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return self.name


class Film(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    cinematographic_genre = models.ManyToManyField(CinematographicGenre)
    director = models.ForeignKey(Person, null=True, blank=True, on_delete=models.SET_NULL)
    score = models.FloatField(null=True, blank=True)
    cover = models.ImageField(upload_to ='uploads/', null=True, blank=True)
    release_date = models.DateField(blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.title