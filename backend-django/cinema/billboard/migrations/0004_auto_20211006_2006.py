# Generated by Django 3.2.8 on 2021-10-06 20:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billboard', '0003_film_year'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='film',
            name='year',
        ),
        migrations.AddField(
            model_name='film',
            name='release_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
