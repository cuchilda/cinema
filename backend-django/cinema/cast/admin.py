from django.contrib import admin
from cast.models import Job, Person


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('position', )
    search_fields = ('position', )
    list_filter = ('position', )


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', )
    search_fields = ('name', 'surname', )
    list_filter = ('name', 'surname', 'job', )