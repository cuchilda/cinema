from django.apps import AppConfig


class CastConfig(AppConfig):
    name = 'cast'

    def ready(self):
        super(CastConfig, self).ready()
