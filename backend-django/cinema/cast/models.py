from django.db import models


class Job(models.Model):
    position = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return self.position


class Person(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    surname = models.CharField(max_length=255, null=False, blank=False)
    birth_date = models.DateField(max_length=255, null=True, blank=True)
    job = models.ManyToManyField(Job)

    def __str__(self):
        return "{} {}".format(self.name, self.surname)
