import React from 'react';
import { createStore } from 'redux';
import { Provider, connect } from 'react-redux';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { store} from './components/counter/configure-store'
import { Container } from './components/counter/container'


ReactDOM.render(
  <React.StrictMode>
    <App>
      <Provider store={store}>
      <Container />
    </Provider>
    </App >
  </React.StrictMode>,
  document.getElementById('root')
);




ReactDOM.render(
  <React.StrictMode>
    <App>
      <Provider store={store}>
      <Container />
    </Provider>
    </App >
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
