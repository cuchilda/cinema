import React, { useState } from "react";

const FormFilm = props => {
    const [name, setName] = useState("");
    const { handleAddItem } = props;
    const handleSubmit = e => {
        e.preventDefault();
        console.log(name);
        handleAddItem({
            done: false,
            name
          });
        setName("");
      };

      
    return (
      <form onSubmit={handleSubmit}>

        <div className="film-list">
          <div className="file-input">
            <input
              type="text"
              className="text"
              value={name}
              onChange={e => setName(e.target.value)}
            />
            <button
              className="button pink"
              disabled={name ? "" : "disabled"}
            >
              Añadir género
            </button>
          </div>
        </div>
      </form>
    );
  };

  export default FormFilm;