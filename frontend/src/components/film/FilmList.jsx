
import React from "react";
import Checkbox from "./Checkbox";


const FilmList = props => {
    const { list, setList } = props;

    const chk = list.map(item => (
        <Checkbox key={item.id} data={item} />
      ));

    return (
    <div className="film-list">
      {list.length ? chk : "No films"}
      {list.length ? (
        <p>
          <button className="button blue">
            Borrar todas
          </button>
        </p>
      ) : null}
    </div>
    );
  };
export default FilmList;