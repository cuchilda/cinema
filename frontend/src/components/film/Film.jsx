import React, { useState } from "react";
import FormFilm from './FormFilm'
import FilmList from './FilmList'

function Film(){
    const [list, setList] = useState([]);

    const handleAddItem = addItem => {
      setList([...list, addItem]);
    };
    return(
        <>
        <h2 > Películas </h2>
        <FormFilm handleAddItem={handleAddItem}/>
        <FilmList list={list} setList={setList}/>
        </ >
    )
}

export default Film