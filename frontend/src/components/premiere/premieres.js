import React from 'react'
import axios from 'axios'
import '../css/card-small.css';
import TopCardList from '../common/top-card-list';



class Premieres extends React.Component {

    state = {
      weekPremiereList : [],
      monthPremiereList: []
    }
  
    componentDidMount() {
      let data 
      axios.get('/api/week-month-premieres')
      .then(res => {
          data = res.data;
          this.setState({
            weekPremiereList : data.weekly_films,    
            monthPremiereList : data.monthly_films  
          });
      })
      .catch(err => {})
    }

  
    render() {
      return(
        <>
            <TopCardList 
                title='Estrenos semanales' 
                filmList={this.state.weekPremiereList} 
            /> 
            <TopCardList 
                title='Estrenos del mes' 
                filmList={this.state.monthPremiereList} 
            /> 
        </>
      )
    }
  }
  
  export default Premieres