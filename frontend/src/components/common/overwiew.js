import React from 'react';
import CardSmall from './card-small';
import '../css/overview.css';


const cardSmallList = [
  {
    icon:'images/icon-facebook.svg',
    pageViews:'87',
    growth: 3,
    key: 1
  },  
  {
    icon:'images/icon-facebook.svg',
    pageViews:'52',
    growth: 3,
    key: 2
  },  
  {
    icon:'images/icon-instagram.svg',
    pageViews:'5462',
    growth: 1375,
    key: 3
  },  
  {
    icon:'images/icon-up.svg',
    pageViews:'117',
    growth: 303,
    key: 4
  },  
]
function Overview(){
    return(
      <section className="overview">
      <div className="wrapper">
        <h2>En cartelera</h2>
        <div className="grid">
          {
            cardSmallList.map(({ icon, key, pageViews, growth}) => (
              <CardSmall 
                icon={icon}
                key={key} 
                pageViews={pageViews} 
                growth={growth} 
              />
            ))
          }
        </div>
      </div>
    </section>
    )
  }

  export default Overview