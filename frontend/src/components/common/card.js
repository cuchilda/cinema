import React from 'react'
import '../css/card.css';

function Card({title, cinematographic_genre, score, cover, year, name}){
    const cardClass = `card instagram`
    const icon = score < 5 ? "images/icon-down.svg" : "images/icon-up.svg"
    const cardScoreClass = score < 5 ? "card-score is-danger" : "card-score"
    const genres = cinematographic_genre.map(({name}) => `${name}`).join(', ');
    console.log(genres)
    return(
        <article className={cardClass}>
            <p className="card-title">
                <img src={cover} alt="" />
                {year}
            </p>
            <p className="card-followers">
                <span className="card-followers-number">{title}</span>
                <span className="card-followers-title">{genres}</span>
            </p>
            <p className={cardScoreClass}>
                <img src={icon} alt="" />
                {score}
            </p>
        </article>
    )
}

export default Card