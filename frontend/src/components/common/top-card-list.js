import React from 'react'
import Card from './card'
import '../css/top-card-list.css';

function TopCardList ({ title, filmList }) {

    console.log(filmList)
    var results = filmList
    return(
        <section className="top-cards">
        <div className="wrapper">
          <h2>{title}</h2>
          <div className="grid">
              {
                  results.map((cardData, id) => <Card key={id} {...cardData}></Card>)
              }
          </div>
        </div>
      </section>
    )
}

export default TopCardList