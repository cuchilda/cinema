import React from 'react'
import '../css/header.css'


function Header({ children }){

  return(
    <header className="header">
      <div className="wrapper">
        <div className="header-grid">
          <div>
            <h1>Cines Cuchillo</h1>

            <p className="header-total">Pongamos manos a la obra</p>
          </div>
          {children}
        </div>
      </div>
    </header>
  )
}

export default Header