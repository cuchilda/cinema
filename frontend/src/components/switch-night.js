import React, {useRef} from 'react'
import './css/switch.css'

function SwitchNigth({setDarkMode, checked, setChecked}) {
    const ref = useRef(null)
    function handleChange(){
        setChecked(ref.current.checked)
        setDarkMode(ref.current.checked)
    }

    return(
        <div className="dark-mode">
            <p className="dark-mode-title">Dark Mode</p>
            <input ref={ref} type="checkbox" className="checkbox" id="checkbox" checked={checked} onChange={handleChange}/>
            <label className="switch" htmlFor="checkbox">
            </label>
        </div>
    )
}

export default SwitchNigth