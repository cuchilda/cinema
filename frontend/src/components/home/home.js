import React from 'react';
import axios from 'axios';
import TopCardList from '../common/top-card-list';
import Overview from '../common/overwiew';


class Home extends React.Component {
  state = {
    weekPremiereList : []
  }

  componentDidMount() {
    let data 
    axios.get('/api/film-list')
    .then(res => {
        data = res.data;
        this.setState({
          weekPremiereList : data    
        });
    })
    .catch(err => {})
  }

  render() {
    return(
      <>
          <TopCardList 
              title='Todas las películas' 
              filmList={this.state.weekPremiereList} 
          />
          <Overview />

      </>
    )
  }
}
  
  export default Home