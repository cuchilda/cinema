import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";
import './globals.css';
import Header from './components/common/header.js'
import SwitchNigth from './components/switch-night';
import Home from './components/home/home';
import Premieres from './components/premiere/premieres';
import Cast from './components/cast/cast'
import Film from './components/film/Film';


function App({ children }) {
  const [darkMode, setDarkMode] = useState(false)
  const [checked, setChecked] = useState(false)
  const mainClass = darkMode ? 'is-dark-mode' : 'is-ligth-mode'


  function changeMedia(mq){
    setDarkMode(mq.matches)
    setChecked(mq.matches)
  }

  function usePromiseEffect(promise){

  }
  useEffect(()=>{
      const mq = window.matchMedia('(prefers-color-scheme: dark)')
      mq.addListener(changeMedia)
      setDarkMode(mq.matches)
      setChecked(mq.matches)
  }, [])
  return (
    <main className={mainClass}>
      <Header >
        <SwitchNigth setDarkMode={setDarkMode} checked={checked} setChecked={setChecked}/>
      </Header>
{/* {children} */}
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
            </ul>            
            <ul>
              <li>
                <Link to="/premieres">Estrenos</Link>
              </li>
            </ul>            
            <ul>
              <li>
                <Link to="/cast">Reparto</Link>
              </li>
            </ul>            
            <ul>
              <li>
                <Link to="/films">Películas</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route exact path="/premieres" component={Premieres}/>
          </Switch>
          <Switch>
            <Route exact path="/" component={Home}/>
          </Switch>
          <Switch>
            <Route exact path="/cast" component={Cast}/>
          </Switch>
          <Switch>
            <Route exact path="/films" component={Film}/>
          </Switch>

        </div>
      </Router>

    </main>
  );
}

export default App;
